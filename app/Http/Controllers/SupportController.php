<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Firebase\JWT\JWT;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    public function index() {
        $user = \Auth::user();
        $jwt = $this->generateJWT($user);

        return redirect('https://software-products-ph.zendesk.com/access/jwt?jwt=' . $jwt);
    }

    public function generateJWT($user) {
        $payload = array(
            'iat' => Carbon::now()->timestamp,
            'jti' => rand(),
            'name' => $user->name,
            'email' => $user->email,
            'external_id' => $user->id,
            'role' => $user->email == "info@software-products.ph" ? "admin" : "agent",
        );

        return JWT::encode($payload, getenv('ZENDESK_SHARED_SECRET'));
    }
}
