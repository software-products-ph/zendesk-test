<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::get('/items', function() {
    $items = collect([
        "Item 1",
        "Item 2",
        "Item 3",
    ]);

    if (Auth::check()) {
        $items->push("Authenticated Item");
    }

    return $items->toJSON();
});


Route::get('/admin-run-migrations', function() {
    return Artisan::call('migrate', ["--force" => true ]);
});



include("Routes/support.php");